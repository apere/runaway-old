// misc.rs
// Author: Alexandre Péré
/// Contains primitives used by librunaway to provide higher level service.

extern crate crypto;
use std::process;
use std::path;
use std::fs;
use std::str;
use std::io::prelude::*;
use self::crypto::digest::Digest;
use super::{Error, SEND_ARCH_RPATH, SEND_IGNORE_RPATH};

/// Execute a given command on a remote host using ssh.
pub fn execute_command_on_remote(ssh_config: &str, command: &str, capture_streams: bool) -> Result<process::Output, Error>{
    // We log
    debug!("Executing command on {}", ssh_config);
    // We retrieve the stdio function
    let stdio_func = match capture_streams{
        true=> process::Stdio::piped,
        false=> process::Stdio::inherit,
    };
    // We perform the command
    let output = process::Command::new("ssh")
        .arg(ssh_config)
        .arg(command)
        .stdin(stdio_func())
        .stdout(stdio_func())
        .stderr(stdio_func())
        .output()?;
    // Depending on the output, we return the corresponding
    match output.status.success(){
        true => return Ok(output),
        false => {
            warn!("Remote execution failed. Command returned {:?}", output);
            return Err(Error::ExecutionFailed(output));
        }
    }
}

/// Pack the files of a directory into a `.tar` archive 
pub fn pack_directory(dir_path: &path::PathBuf) -> Result<(), Error>{
    // We log
    debug!("Packing directory {}", dir_path.to_str().unwrap());
    // We perform the command
    let tar_output = process::Command::new("tar")
        .arg("cf")
        .arg(SEND_ARCH_RPATH)
        .arg("-X")
        .arg(SEND_IGNORE_RPATH)
        .args(dir_path.read_dir().unwrap().into_iter().map(|x| x.unwrap().file_name()).collect::<Vec<_>>())
        .current_dir(dir_path)
        .output()?;
    // Depending on the output, we return the corresponding
    match tar_output.status.success(){
        true => return Ok(()),
        false => {
            warn!("Packing of directory failed. Command returned {:?}",tar_output);
            return Err(Error::Packing)
        }
    }
}

/// Unpack the content of a `.tar` archive into a directory.
pub fn unpack_archive(archive_path: &path::PathBuf) -> Result<(), Error>{
    // We log
    debug!("Unpacking archive {}", archive_path.to_str().unwrap());
    // We perform the command
    let tar_output = process::Command::new("tar")
        .arg("xf")
        .arg(archive_path.to_str().unwrap())
        .current_dir(archive_path.parent().unwrap())
        .output()?;
    // Depending on the output, we return the corresponding
    match tar_output.status.success(){
        true => return Ok(()),
        false => {
            warn!("Unpacking of archive failed. Command returned {:?}",tar_output);
            return Err(Error::Unpacking)
        }
    }
}

/// Computes SHA2-256 hash of a file
pub fn compute_file_hash(file_path: &path::PathBuf) -> Result<String, Error>{
    // We log
    debug!("Computing hash of {}", file_path.to_str().unwrap());
    // We open the file
    let mut file = fs::File::open(file_path)?;
    // We instantiate hasher
    let mut hasher = crypto::sha2::Sha256::new();
    // We read file
    let mut file_content = Vec::new();
    file.read_to_end(&mut file_content)?;
    // We hash
    hasher.input(&file_content);
    // We return
    return Ok(hasher.result_str());
}

/// Sends a file to a remote host
pub fn send_file(local_file_path: &path::PathBuf, ssh_config: &str, host_file_path: &path::PathBuf) -> Result<(), Error>{
    // We log
    debug!("Sending file {} to {}:{}", local_file_path.to_str().unwrap(), ssh_config, host_file_path.to_str().unwrap());
    // We perform the command
    let scp_output = process::Command::new("scp")
        .arg(local_file_path.to_str().unwrap())
        .arg(format!("{}:{}", ssh_config, host_file_path.to_str().unwrap()))
        .output()?;
    // Depending on the output, we return the corresponding
    match scp_output.status.success(){
        true => return Ok(()),
        false => {
            warn!("Sending file failed. Command returned: {:?}",scp_output);
            return Err(Error::ScpSend);
        }
    }
}

/// Fetch a file from a remote host
pub fn fetch_file(local_file_path: &path::PathBuf, ssh_config: &str, host_file_path: &path::PathBuf) -> Result<(), Error>{
    // We log
    debug!("Fetching file {}:{} to {}", ssh_config, host_file_path.to_str().unwrap(), local_file_path.to_str().unwrap());
    // We perform the command
    let scp_output = process::Command::new("scp")
        .arg(format!("{}:{}", ssh_config, host_file_path.to_str().unwrap()))
        .arg(local_file_path.to_str().unwrap())
        .output()?;
    // Depending on the output, we return the corresponding
    match scp_output.status.success(){
        true => return Ok(()),
        false => {
            warn!("Fetching file failed. Command returned {:?}", scp_output);
            return Err(Error::ScpFetch);
        }
    }
}

#[cfg(test)]
mod tests {
    use std::fs;
    use super::*;

    // CONSTANTS
    static TEST_PATH: &str = include_str!("../test/constants/test_path");

    #[test]
    fn test_execute_remote_command(){
        let test_path = path::PathBuf::from(TEST_PATH).join("hostprofile");
        if test_path.join("touch.md").exists(){
            fs::remove_file(test_path.join("touch.md"));
        }
        execute_command_on_remote("localhost", format!("cd {} && touch touch.md", test_path.to_str().unwrap()).as_str(), false);
        assert!(test_path.join("touch.md").exists());
        let output = execute_command_on_remote("localhost", "echo stdout && >&2 echo stderr ", true).unwrap();
        let stdout = String::from_utf8(output.stdout).unwrap();
        let stderr = String::from_utf8(output.stderr).unwrap();
        assert_eq!(stdout.as_str(), "stdout\n");
        assert_eq!(stderr.as_str(), "stderr\n");
    }

    #[test]
    fn test_pack_unpack(){
        let test_path = path::PathBuf::from(TEST_PATH).join("pack_unpack");
        if test_path.join("recovered").exists(){
            fs::remove_dir_all(test_path.join("recovered"));
        }
        fs::create_dir(test_path.join("recovered"));
        pack_directory(&test_path.join("original")).expect("error");
        fs::copy(test_path.join("original/.send.tar"), test_path.join("recovered/.send.tar")).unwrap();
        unpack_archive(&test_path.join("recovered/.send.tar")).unwrap();
        assert!(test_path.join("recovered/1.md").exists());
        assert!(test_path.join("recovered/2.md").exists());
        assert!(!test_path.join("recovered/ignored/3.md").exists());
    }

    #[test]
    fn test_hash(){
        let test_path = path::PathBuf::from(TEST_PATH).join("hash");
        let hash = compute_file_hash(&test_path.join("file")).unwrap();
        assert_eq!(hash, "32a5bd248fcbe43faf368367176e6818b432d418f5ebdf98f1c256371bc6316f");
    }

    #[test]
    fn test_send_fetch(){
        let test_path = path::PathBuf::from(TEST_PATH).join("send_fetch");
        if test_path.join("local/remote_file").exists(){
            fs::remove_file(test_path.join("local/remote_file")).unwrap();
        }
        if test_path.join("remote/local_file").exists(){
            fs::remove_file(test_path.join("remote/local_file")).unwrap();
        }
        send_file(&test_path.join("local/local_file"), "localhost", &test_path.join("remote/local_file")).unwrap();
        fetch_file(&test_path.join("local/remote_file"), "localhost", &test_path.join("remote/remote_file")).unwrap();
        assert!(test_path.join("local/remote_file").exists());
        assert!(test_path.join("remote/local_file").exists());
    }
}


